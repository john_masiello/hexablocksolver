
# Solving the Hexa Block Puzzle Using Optimization

## Conditions 
- cond_0 does the block fit into into the grid at (x, y)


- cond_1 do all of the other blocks fit into the remaining grid (islands) & cond_0*


- cond_2 both cond_1 & (x, y) maximizes shoreline boundary. On tiebreak pick (x, y) that induces the fewest number of islands.

</br>

<p>* It may be that all other blocks need not fit if the sum of their cells exceeds
 the grid cell count. We will need some soft modifications for that.</p>

## Algorithm

<ol>
<li> Select first the block with the most number of cells that satisfies cond_1. 
On a tiebreak on cellcount, pick block that also satisfies cond_2. On tiebreak pick block that induces the fewest number of islands.</li>

</br>

<li> Choose the initial (x, y). If block selection went to tiebreaks, it may be that we already know (x, y), otherwise, we select (x, y) by comparisons done over iteration. Each (x, y) should be rank-ordered, according to the optimization as already specified.</li>

</br>

<li> Place the block into the grid. Remove all cells that intersect. Remove the block from the list of remaining blocks.</li>

</br>

<li> Repeat steps 1-4 until either the grid has no more empty cells remaining or none of the remaining blocks fit. For the latter case, we first revert our choice of (x, y) then our choice of block, if we have determined the block is unnecessary. We repeat steps 1-4 observing the rank orderings (step 2) until all choices are exhausted.</li>
</ol>