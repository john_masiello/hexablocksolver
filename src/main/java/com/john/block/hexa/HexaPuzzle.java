package com.john.block.hexa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.mutable.MutableInt;

import com.john.block.hexa.core.BlockMoveIslandComparator;
import com.john.block.hexa.core.BlockMoveShorelineComparator;
import com.john.block.hexa.core.HexaBlock;
import com.john.block.hexa.core.HexaGrid;
import com.john.block.hexa.core.HexaPuzzleGrid;
import com.john.block.hexa.core.HexaShape;
import com.john.block.hexa.core.HexaShapeCellsComparator;
import com.john.block.hexa.core.HexaShapeOps;
import com.john.block.hexa.model.BlockMove;
import com.john.block.hexa.model.BlockMoveNode;
import com.john.block.hexa.model.BlockSolution;
import com.john.block.hexa.util.SkipArray;
import com.john.block.hexa.util.SkipArrayIterator;
import com.john.block.hexa.util.Utility;

public class HexaPuzzle {
	// properties
	private final HexaPuzzleGrid puzzleGrid;
	
	private final SkipArray<HexaBlock> blocks;
	
	private final int superfluousCellCount;
	
	private final int gridCellCount;
	
	// state
	private SkipArrayIterator<HexaBlock> skipit;

	private BlockMoveShorelineComparator shorelineComparator;

	private BlockMoveIslandComparator islandComparator;

	// constants
	private static final int OK 						= 0;
	private static final int NO_MORE_BLOCKS 			= 1;
	private static final int BLOCK_DOES_NOT_FIT			= 2;

	
	public HexaPuzzle(HexaPuzzleGrid puzzleGrid, HexaBlock... blocks) {
		this.puzzleGrid = new HexaPuzzleGrid(Utility.deepCopy(puzzleGrid.getHexaGrids(), this::gridGen));
		
		HexaBlock[] arrBlocks = Utility.deepCopy(blocks, this::blockGen);
		Arrays.sort(arrBlocks, new HexaShapeCellsComparator());
		this.blocks = new SkipArray<>(arrBlocks);
		
		gridCellCount = computeAggregateCellCount(puzzleGrid.getHexaGrids());
		superfluousCellCount = computeAggregateCellCount(this.blocks) - gridCellCount;
	} 
	
	private HexaBlock blockGen(HexaBlock block) {
		return new HexaBlock(block.getCells());
	}
	
	private HexaGrid gridGen(HexaGrid grid) {
		return new HexaGrid(grid.getCells());
	}

	public HexaPuzzleGrid getPuzzleGrid() {
		return new HexaPuzzleGrid(Utility.deepCopy(puzzleGrid.getHexaGrids(), this::gridGen));
	}

	public SkipArray<HexaBlock> getBlocks() {
		return new SkipArray<>(Utility.deepCopy(blocks.getArray(), this::blockGen));
	}
	
	private int computeAggregateCellCount(Iterable<? extends HexaShape> hexaShapes) {
		int extraCellCount = 0;
		
		for (HexaShape shape : hexaShapes) {
			extraCellCount += shape.getNumberOfCells();
		}
		return extraCellCount;
	}

	/**
	 * 
	 * @param grid
	 * @param block
	 * @param columnIndex
	 * @return the starting row index for checking the block fits into the grid
	 */
	private int findStartRowIndex(HexaShape grid, HexaBlock block, int columnIndex) {
		return (grid.getParity() + block.getParity() + columnIndex) % 2;
	}
	
	private boolean isColumnIndexInRange(HexaShape grid, HexaBlock block, int columnIndex) {
		return block.getWidth() + columnIndex <= grid.getWidth();
	}

	private boolean isRowIndexInRange(HexaShape grid, HexaBlock block, int rowIndex) {
		return block.getHeight() + rowIndex <= grid.getHeight();
	}
	
	/**
	 * 
	 * @param grid
	 * @param block
	 * @param rowIndex
	 * @param colIndex
	 * @return whether the block fits into grid at (rowIndex, colIndex)
	 */
	boolean doesFit(HexaGrid grid, HexaBlock block, int rowIndex, int colIndex) {
		boolean [][] blockCells = block.getCells();
		boolean [][] gridCells = grid.getCells();
		int blockRowHeight = blockCells.length;
		int blockColWidth = blockCells[0].length;
		int blockParity = block.getParity();
		
		for (int j = 0; j < blockColWidth; j++) {
			for (int i = (blockParity + j) % 2; i < blockRowHeight; i += 2 ) {
				if (blockCells[i][j] && 
						!gridCells[rowIndex + i][colIndex + j])
					return false;
			}
		}
		return true;
	}
	
	List<BlockMove> findAllPositions(final int blockIndex) {
		List<BlockMove> result = new ArrayList<>();
		HexaBlock block = blocks.get(blockIndex);
		BlockMove blockMove;
		
		int rowIndex, colIndex;
		for (HexaGrid grid : puzzleGrid) {
			colIndex = 0;
			while (isColumnIndexInRange(grid, block, colIndex)) {
				rowIndex = findStartRowIndex(grid, block, colIndex);
				while (isRowIndexInRange(grid, block, rowIndex)) {
					if (doesFit(grid, block, rowIndex, colIndex)) {
						blockMove = new BlockMove()
								.setBlock(block)
								.setBlockIndex(blockIndex)
								.setGrid(grid)
								.setRow(rowIndex)
								.setCol(colIndex);
						result.add(blockMove);
					}
					rowIndex += 2;
				}
				colIndex++;
			}
		}
		
		return result;
	}
	
	boolean doesBlockFitInPuzzle(HexaBlock block) {		
		int rowIndex, colIndex;
		
		for (HexaGrid grid : puzzleGrid) {
			colIndex = 0;
			while (isColumnIndexInRange(grid, block, colIndex)) {
				rowIndex = findStartRowIndex(grid, block, colIndex);
				while (isRowIndexInRange(grid, block, rowIndex)) {
					if (doesFit(grid, block, rowIndex, colIndex)) {
						return true;
					}
					rowIndex += 2;
				}
				colIndex++;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param blockMove the coordinate of the block pointed to by skipit.peek()
	 * @return the array index of the currentBlock
	 */
	private void placeBlockInGrid(BlockMove blockMove) {
		HexaShapeOps.xor(blockMove.getGrid(), 
				blockMove.getBlock(), 
				blockMove.getRow(), 
				blockMove.getCol());	
		skipit.skipAt(blockMove.getBlockIndex());
	}
	
	private void removeBlockFromGrid(BlockMove blockMove) {
		HexaShapeOps.xor(blockMove.getGrid(), 
				blockMove.getBlock(), 
				blockMove.getRow(), 
				blockMove.getCol());	
		skipit.restoreAt(blockMove.getBlockIndex());
	}
	
	private boolean doesBlockImpedeRequiredBlocks(BlockMove blockMove) {
		placeBlockInGrid(blockMove);
		int wastedCellCount = 0;
		HexaBlock block;
		skipit.head();
		
		boolean result = false;
		
		while (skipit.hasNext()) {
			block = skipit.next();
			if (doesBlockFitInPuzzle(block))
				continue;
			wastedCellCount += block.getNumberOfCells();
			if (wastedCellCount > superfluousCellCount) {
				result = true;
				break;
			}
		}
		removeBlockFromGrid(blockMove);
		return result;
	}
	
	private void computeShoreLineImpact(List<BlockMove> blockMoves) {
		blockMoves.forEach(move -> {
			placeBlockInGrid(move);
			int shoreLineImpact = -move.getGrid().getShoreLineCount();
			move.setShorelineImpact(shoreLineImpact);
			removeBlockFromGrid(move);
		});
	}
	
	private void computeIslandImpact(BlockMove blockMove) {
		placeBlockInGrid(blockMove);
		int islandImpact = -blockMove.getGrid().getIslandCount();
		blockMove.setIslandImpact(islandImpact);
		removeBlockFromGrid(blockMove);
	}
	
	
	
	private int findAggregateMoves(ArrayList<BlockMove> aggMoves, 
			SkipArrayIterator<HexaBlock> tieBlocks,
			MutableInt cellCost) {
		
		int blockCellCount;
		int blockIndex;
		aggMoves.clear();
		List<BlockMove> moves; 
		HexaBlock block, peek;
		
		peek = tieBlocks.peek();
		
		BLOCK_LOOP:
		while (tieBlocks.hasNext() && aggMoves.isEmpty()) {
			blockCellCount = peek.getNumberOfCells();
			
			while (tieBlocks.hasNext()) {
				peek = tieBlocks.peek();
				if (peek.getNumberOfCells() != blockCellCount)
					continue BLOCK_LOOP;
				
				block = tieBlocks.next();				
				blockIndex = tieBlocks.position();	
				peek = tieBlocks.peek();
									
				if (!doesBlockFitInPuzzle(block)) {
					if (cellCost.addAndGet(block.getNumberOfCells()) > superfluousCellCount)
						return BLOCK_DOES_NOT_FIT;
					continue;
				}
				
				moves = findAllPositions(blockIndex);
				moves.removeIf(this::doesBlockImpedeRequiredBlocks);
				
				if (moves.isEmpty()) {
					if (cellCost.addAndGet(block.getNumberOfCells()) > superfluousCellCount)
						return BLOCK_DOES_NOT_FIT;
					continue;
				}
				
				aggMoves.addAll(moves);
			}
		}
		if (aggMoves.isEmpty())
			return NO_MORE_BLOCKS;
		
		computeShoreLineImpact(aggMoves);
		Collections.sort(aggMoves, shorelineComparator);
		return OK;
	}
	
	private void partialComputeIslandImpact(ArrayList<BlockMove> aggMoves, 
			MutableInt aggregateMoveIndex, 
			MutableInt aggregateSortedIndex) {
		
		int shoreline;
		int numberOfMoves;
		
		numberOfMoves = aggMoves.size();
		
		shoreline = aggMoves.get(aggregateMoveIndex.getValue()).getShorelineImpact();
		
		for (int i = aggregateMoveIndex.getValue() + 1; i < numberOfMoves; i++) {
			if (aggMoves.get(i).getShorelineImpact() == shoreline) {
				computeIslandImpact(aggMoves.get(i));
				aggregateSortedIndex.setValue(i);;
				continue;
			}
			break;
		}
		if (aggregateSortedIndex.compareTo(aggregateMoveIndex) > 0) {
			computeIslandImpact(aggMoves.get(aggregateMoveIndex.getValue()));
			Collections.sort(
					aggMoves.subList(aggregateMoveIndex.getValue(), 
							aggregateSortedIndex.getValue() + 1),
					islandComparator);
		}
		// The sorted index is always exclusive of the range of aggregateMoveIndex
		aggregateSortedIndex.increment();
	}
	
	private int lazyNextMove(BlockMoveNode blockMoveNode) {

		ArrayList<BlockMove> blockMoves = blockMoveNode.getAggMoves();
		MutableInt moveIndex = blockMoveNode.getAggregateMoveIndex();
		MutableInt sortedIndex = blockMoveNode.getAggregateSortedIndex();
		MutableInt cellCost = blockMoveNode.getCellCost();
		SkipArrayIterator<HexaBlock> it = blockMoveNode.getTieBreakIterator();
		
		int moveStatus;
		
		if (moveIndex.equals(sortedIndex)) {
			if (sortedIndex.getValue() == blockMoves.size()) {
				moveStatus = findAggregateMoves(blockMoves, it, cellCost);
				if (moveStatus == OK) {
					moveIndex.setValue(0);
					sortedIndex.setValue(0);
					
				} else {
					return moveStatus;
				}
			}
			partialComputeIslandImpact(blockMoves, moveIndex, sortedIndex);
		}
		int nextIndex = moveIndex.getAndIncrement();
		blockMoveNode.setBlockMove(blockMoves.get(nextIndex));
		return OK;
	}
	
	/**
	 * 
	 * @return A list of block of coordinates for each block, following the Block Solution model,
	 * if there exists a solution; otherwise, null
	 * 
	 * @see com.john.block.hexa.model.BlockSolution BlockSolution
	 */
	public List<? extends BlockSolution> solve() {
		LinkedList<BlockMoveNode> movesStack = new LinkedList<>();
		BlockMoveNode blockMoveNode;
		skipit = blocks.skipIterator();
		shorelineComparator = new BlockMoveShorelineComparator();
		islandComparator = new BlockMoveIslandComparator();
		
		int totalFittedCellCount = 0;
		int moveStatus;
		
		// All grid cells must be filled
		while (totalFittedCellCount != gridCellCount) {
			
			blockMoveNode = new BlockMoveNode();
			blockMoveNode.setTieBreakIterator(skipit.shallowCopy());
			blockMoveNode.getTieBreakIterator().head();
			
			moveStatus = lazyNextMove(blockMoveNode);
			
			while (moveStatus != OK) {
				if (movesStack.isEmpty()) {
					// No solution
					return null;
				}
				blockMoveNode = movesStack.pollLast();
				removeBlockFromGrid(blockMoveNode.getBlockMove());
				totalFittedCellCount -= blockMoveNode.getBlockMove().getBlock().getNumberOfCells();
				moveStatus = lazyNextMove(blockMoveNode);
			}
			
			// Accept the block move
			placeBlockInGrid(blockMoveNode.getBlockMove());
			movesStack.offer(blockMoveNode);
			totalFittedCellCount += blockMoveNode.getBlockMove().getBlock().getNumberOfCells();
		}
		
		// The problem is solved

		return movesStack.stream()
				.map(node -> node.getBlockMove())
				.map(move -> {
					removeBlockFromGrid(move);
					return move;
					
				}).collect(Collectors.toList());
	}
}