package com.john.block.hexa.core;

/**
 * 
 * Should be used as mutable
 *
 * @author John Masiello
 *
 */
public class HexaGrid extends HexaShape {
	
	private IslandCounter islandCounter;

	public HexaGrid(boolean[][] cells) {
		super(cells);
	}
	
	{
		islandCounter = new IslandCounter(this);
	}

	@Override
	public boolean[][] getCells() {
		return getCellsReference();
	}

	@Override
	public int getNumberOfCells() {
		return countCells();
	}

	@Override
	public int getShoreLineCount() {
		return computeshoreLineCount();
	}
	
	public int getIslandCount() {
		return islandCounter.findIslandCount();
	}
}
