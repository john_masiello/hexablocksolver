package com.john.block.hexa.core;

import java.util.Comparator;

public class HexaShapeCellsComparator implements Comparator<HexaShape> {

	public int compare(HexaShape o1, HexaShape o2) {
		return o2.getNumberOfCells() - o1.getNumberOfCells();
	}

}