package com.john.block.hexa.core;

public class HexaShapeOps {
	
	public static void xor(HexaGrid on, HexaShape by, int rowIndex, int colIndex) {
		boolean [][] byCells = by.getCells();
		boolean [][] onCells = on.getCells();
		int blockRowHeight = byCells.length;
		int blockColWidth = byCells[0].length;
		int blockParity = by.getParity();
		
		for (int j = 0; j < blockColWidth; j++) {
			for (int i = (blockParity + j) % 2; i < blockRowHeight; i += 2 ) {
				if (byCells[i][j]) 
					onCells[rowIndex + i][colIndex + j] ^= true;
			}
		}
	}
	
	public static boolean isEmpty(HexaGrid grid) {
		boolean [][] cells = grid.getCells();
		int blockRowHeight = cells.length;
		int blockColWidth = cells[0].length;
		int blockParity = grid.getParity();
		
		for (int j = 0; j < blockColWidth; j++) {
			for (int i = (blockParity + j) % 2; i < blockRowHeight; i += 2 ) {
				if (cells[i][j])
					return false;
			}
		}
		return true;
	}
}
