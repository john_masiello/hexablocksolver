package com.john.block.hexa.core;

import java.awt.Point;
import java.util.LinkedList;

class IslandCounter {
	
	private LinkedList<Point> deque;
	
	private LinkedList<Point> recycled;
	
	private HexaShape hexaShape;
	
	boolean[][] cells;	
	
	private boolean[][] unmarkedCells;
	
	public IslandCounter(HexaShape hexaShape) {
		if (hexaShape == null)
			throw new IllegalStateException("HexaShape cannot be null");
		this.hexaShape = hexaShape;
	}

	{
		deque = new LinkedList<>();
		recycled = new LinkedList<>();
		unmarkedCells = new boolean[][] {};
	}
	
	public int findIslandCount() {
		int islandCount = 0;
		
		cells = hexaShape.getCells();
		resetUnMarkedCells(cells);
		Point hexa;
		
		while (offerAnchorHexa()) {
			islandCount++;
			hexa = pollHexa();
			
			while (hexa != null) {
				accumulateAdjacentCells(hexa);
				hexa = pollHexa();
			}
		}
		return islandCount;
	}
	
	private void resetUnMarkedCells(boolean[][] cells) {
		int height = cells.length;
		int width = cells[0].length;
		
		if (unmarkedCells.length != height ||
				unmarkedCells[0].length != width)
			unmarkedCells = new boolean[height][width];
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				unmarkedCells[i][j] = true;
			}
		}
	}
	
	private void accumulateAdjacentCells(Point hexa) {
		int x = hexa.x;
		int y = hexa.y;
		
		int height = cells.length;
		int width = cells[0].length;
		
		if (x > 1 && isHexaFree(x - 2, y))
			markAndOfferHexa(x - 2, y);
		
		if (x + 2 < height && isHexaFree(x + 2, y))
			markAndOfferHexa(x + 2, y);
		
		if (x > 0 && y > 0 && isHexaFree(x - 1, y - 1))
			markAndOfferHexa(x - 1, y - 1);
		
		if (x > 0 && y + 1 < width && isHexaFree(x - 1, y + 1))
			markAndOfferHexa(x - 1, y + 1);
		
		if (x + 1 < height && y + 1 < width && isHexaFree(x + 1, y + 1))
			markAndOfferHexa(x + 1, y + 1);
		
		if (x + 1 < height && y > 0 && isHexaFree(x + 1, y - 1))
			markAndOfferHexa(x + 1, y - 1);
		
	}
	
	private boolean offerAnchorHexa() {
		int height = cells.length;
		int width = cells[0].length;
		int parity = hexaShape.getParity();

		for (int j = 0; j < width; j++) {
			for (int i = (parity + j) % 2; i < height; i += 2) {
				if (isHexaFree(i, j)) {
					markAndOfferHexa(i, j);
					return true;
				}
			}
		}
		return false;
	}
	
	private void markAndOfferHexa(int x, int y) {
		Point point;
		
		if (recycled.isEmpty())
			point = new Point(x, y);
		else {
			point = recycled.poll();
			point.x = x;
			point.y = y;
		}
		deque.offer(point);
		unmarkedCells[x][y] = false;
	}
	
	private Point pollHexa() {
		Point point = deque.poll();
		if (point != null)
			recycled.offer(point);
		return point;
	}
	
	private boolean isHexaFree(int x, int y) {
		return cells[x][y] && unmarkedCells[x][y];
	}
}
