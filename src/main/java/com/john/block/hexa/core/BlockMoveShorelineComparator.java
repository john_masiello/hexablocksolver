package com.john.block.hexa.core;

import java.util.Comparator;

import com.john.block.hexa.model.BlockMove;

public class BlockMoveShorelineComparator implements Comparator<BlockMove> {

	public int compare(BlockMove o1, BlockMove o2) {
		return o2.getShorelineImpact() - o1.getShorelineImpact();
	}

}