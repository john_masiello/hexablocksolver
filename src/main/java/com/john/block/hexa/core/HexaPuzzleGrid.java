package com.john.block.hexa.core;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class HexaPuzzleGrid implements Iterable<HexaGrid> {

	private List<HexaGrid> hexaGrids;

	public HexaPuzzleGrid(HexaGrid... hexaGrids) {
		this.hexaGrids = Arrays.asList(hexaGrids);
	}
	
	public HexaPuzzleGrid(List<HexaGrid> hexaGrids) {
		this.hexaGrids = hexaGrids;
	}

	public List<HexaGrid> getHexaGrids() {
		return hexaGrids;
	}

	@Override
	public Iterator<HexaGrid> iterator() {
		return hexaGrids.iterator();
	}
}
