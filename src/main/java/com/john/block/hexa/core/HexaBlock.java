package com.john.block.hexa.core;

/**
 * 
 * Should be used as immutable
 *
 * @author John Masiello
 *
 */
public class HexaBlock extends HexaShape {

	public HexaBlock(boolean[][] cells) {
		super(cells);
	}

	@Override
	public boolean[][] getCells() {
		return getCellsReference();
	}
}
