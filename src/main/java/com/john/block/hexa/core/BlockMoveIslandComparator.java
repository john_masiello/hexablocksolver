package com.john.block.hexa.core;

import java.util.Comparator;

import com.john.block.hexa.model.BlockMove;

public class BlockMoveIslandComparator implements Comparator<BlockMove> {

	public int compare(BlockMove o1, BlockMove o2) {
		return o2.getIslandImpact() - o1.getIslandImpact();
	}

}