package com.john.block.hexa.core;

import com.john.block.hexa.util.Utility;

/**
 * 
 * Immutable representation of a shape made of hexa-units
 * 
 * @author John Masiello
 *
 */
public abstract class HexaShape {
	private final int numberOfCells;
	
	private final boolean[][] cells;
	
	private final int width;
	
	private final int height;
	
	private final int parity;
	
	private final int shoreLineCount;

	/**
	 * 
	 * @param cells Use a checkerboard pattern to represent hex cells. In other words,
	* use a sparse rectangular array
	* A value of true represents the top half of a hexagonal unit.
	* For counting all hexagonal units, it suffices to count their top halves.
	* For height we must add one to count for the complementary the bottom half.
	* Top and bottom rows and left and right columns must each not be empty,
	* or else an IllegalStateException is thrown.
	* 
	* @see java.lang.IllegalStateException
	 */
	public HexaShape(boolean[][] cells) {
		if (!isTightlyBounded(cells))
			throw new IllegalStateException("Cells are not tightly bound");
		if (!isObservingCheckerBoard(cells))
			throw new IllegalStateException("Cells do not obey checker board");
		this.cells = Utility.copyCells(cells);;
		this.width = cells[0].length;
		this.height = cells.length + 1;
		this.numberOfCells = countCells();
		this.parity = findNonEmptyParity(cells);
		this.shoreLineCount = computeshoreLineCount();
	}

	protected int countCells() {
		int count = 0;
		for (boolean[] slice : cells)
			for (boolean cell : slice)
				if (cell)
					count++;
		return count;
	}
	
	protected int computeshoreLineCount() {
		int blockRowHeight = cells.length;
		int blockColWidth = cells[0].length;
		int shoreline = 0;
		
		for (int j = 0; j < blockColWidth; j++) {
			for (int i = (parity + j) % 2; i < blockRowHeight; i += 2 ) {
				if (cells[i][j]) {
					if (i < 2 || !cells[i - 2][j])
						shoreline++;
					if (i + 3 > blockRowHeight || !cells[i + 2][j])
						shoreline++;
					if (i < 1 || j < 1 || !cells[i - 1][j - 1])
						shoreline++;
					if (i < 1 || j + 2 > blockColWidth || !cells[i - 1][j + 1])
						shoreline++;
					if (i + 2 > blockRowHeight || j + 2 > blockColWidth || !cells[i + 1][j + 1])
						shoreline++;
					if (i + 2 > blockRowHeight || j < 1 || !cells[i + 1][j - 1])
						shoreline++;
				}
			}
		}
		return shoreline;
	}
	
	private boolean isTightlyBounded(boolean[][] cells) {
		if (cells.length == 0)
			return false;
		
		boolean leftBound, rightBound, topBound, bottomBound;
		leftBound = rightBound = topBound = bottomBound = false;
		int colLength;
		final int maxRowIndex = cells.length - 1;
		
		for (int i = 0; i < cells.length; i++) {
			colLength = cells[i].length;
			if (colLength == 0)
				return false;
			
			if (i == 0 || i == maxRowIndex) {
				for (int j = 0; j < colLength; j++) {
					if (cells[i][j]) {
						if (i == 0)
							topBound = true;
						if (i == maxRowIndex)
							bottomBound = true;
						break;
					}
				}
			}
			leftBound |= cells[i][0];
			rightBound |= cells[i][colLength - 1];
		}
		return leftBound && rightBound && topBound && bottomBound;
	}
	
	// Current implementation requires cells isTightlyBounded(cells)
	private boolean isObservingCheckerBoard(boolean[][] cells) {
		int emptyParity = 1 - findNonEmptyParity(cells);
		
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				if (cells[i][j] &&
						(i + j) % 2 == emptyParity)
					return false;
			}
		}
		return true;
	}
	
	protected int findNonEmptyParity(boolean[][] cells) {
		int nonEmptyParity = 0;
		
		boolean[] row = cells[0];
		for (int i = 0; i < row.length; i++) {
			if (row[i]) {
				nonEmptyParity = i % 2;
				break;
			}
		}
		return nonEmptyParity;
	}

	public int getNumberOfCells() {
		return numberOfCells;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getParity() {
		return parity;
	}

	public boolean[][] getCells() {
		return Utility.copyCells(cells);
	}
	
	protected boolean[][] getCellsReference() {
		return cells;
	}

	public int getShoreLineCount() {
		return shoreLineCount;
	}
}