package com.john.block.hexa.util;

import java.util.Iterator;

public class SkipArrayIterator<T> implements Iterator<T> {
	
	private int currentIndex;
	
	private int nextIndex;
	
	
	private SkipArray<T> skipArray;
	
	private boolean[] skipped;

	public SkipArrayIterator(SkipArray<T> skipArray) {
		this.skipArray = skipArray;
		skipped = new boolean[skipArray.size()];
		head();
	}
	
	public SkipArrayIterator<T> shallowCopy() {
		SkipArrayIterator<T> cpy = new SkipArrayIterator<>(skipArray);
		cpy.skipped = skipped;
		cpy.currentIndex = currentIndex;
		cpy.nextIndex = nextIndex;
		return cpy;
	}
	
	private void updateNextIndex() {
		while (nextIndex < skipArray.size()) {
			if (!skipped[nextIndex] && nextIndex > currentIndex)
				break;
			nextIndex++;
		}
	}

	@Override
	public boolean hasNext() {
		if (skipArray == null)
			return false;
		updateNextIndex();
		return nextIndex < skipArray.size();
	}

	@Override
	public T next() {
		updateNextIndex();
		currentIndex = nextIndex;
		return skipArray.get(currentIndex);
	}
	
	/**
	 * 
	 * @return next element without advancing the iterator
	 */
	public T peek() {
		return hasNext() ? skipArray.get(nextIndex) :
			null;
	}
	
	/**
	 * 
	 * @return the position, or index, of the most recent element returned by next();
	 * 
	 * @see #next()
	 */
	public int position() {
		return currentIndex;
	}

	public void head() {
		currentIndex = -1;
		nextIndex = 0;
	}
	
	public void restoreAll() {
		for (int i = 0; i < skipped.length; i++)
			skipped[i] = false;
	}
	
	public void restoreAt(int index) {
		skipped[index] = false;
	}
	
	public void skipAt(int index) {
		skipped[index] = true;
	}
	
	public void skip() {
		if (currentIndex != -1)
			skipped[currentIndex] = true;
	}
	
	public void skipNext() {
		updateNextIndex();
		skipped[nextIndex] = true;
	}
}
