package com.john.block.hexa.util;

import java.util.Arrays;
import java.util.Iterator;

public class SkipArray<E> implements Iterable<E> {

	private E[] array;
	
	public SkipArray(E[] array) {
		this.array = Arrays.copyOf(array, array.length);
	}
	
	public void set(int index, E value) {
		array[index] = value;
	}
	
	public E get(int index) {
		return array[index];
	}
	
	public int size() {
		return array.length;
	}
	
	public E[] getArray() {
		return array;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new SkipArrayIterator<E>(this);
	}
	
	public SkipArrayIterator<E> skipIterator() {
		return new SkipArrayIterator<E>(this);
	}
}
