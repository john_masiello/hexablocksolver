package com.john.block.hexa.util;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Utility {
	/**
	 * 
	 * @param binaryCells binary string of 1's and 0's where 1 is nonempty,
	 * rows are separated by a ';', and
	 * the rows give a checkerboard representation of shape in hexagonal units.
	 * @return
	 */
	public static boolean[][] cellsFromBinaryString(String binaryCells) {
		int columns = 0;
		String[] rows = binaryCells.split(";");
		for (String row : rows) {
			if (row.length() > columns)
				columns = row.length();
		}
		
		boolean[][] cells = new boolean[rows.length][columns];
		
		for (int j = 0; j < rows.length; j++) {
			for (int i = 0; i < rows[j].length(); i++)
				cells[j][i] = rows[j].charAt(i) == '1';
		}
		return cells;
	}
	
	public static String printCells(boolean[][] cells ) {
		StringBuilder builder = new StringBuilder();
		for (boolean[] slice : cells) {
			for (boolean cell : slice) {
				builder.append(cell ? '1' : '0');
			}
			builder.append(';');
		}
		return builder.substring(0, builder.length() - 1);
	}
	
	public static boolean[][] copyCells(boolean[][] cells) {
		boolean[][] cpyCells = new boolean[cells.length][];
		for (int i = 0; i < cells.length; i++) {
			cpyCells[i] = Arrays.copyOf(cells[i], cells[i].length);
		}
		return cpyCells;
	}
	
	public static <T> List<T> deepCopy(List<T> items, Function<T, T> supplier) {
		return StreamSupport.stream(items.spliterator(), false)
			.map(item -> supplier.apply(item))
			.collect(Collectors.toList());
	}
	
	public static <T> T[] deepCopy(T[] items, Function<T, T> supplier) { 
		T[] cpy = Arrays.copyOf(items, items.length);
		return (T[]) Stream.of(items)
				.map(item -> supplier.apply(item))
				.collect(Collectors.toList())
				.toArray(cpy);
	}
}
