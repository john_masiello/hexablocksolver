package com.john.block.hexa.model;

import com.john.block.hexa.core.HexaBlock;
import com.john.block.hexa.core.HexaGrid;
import com.john.block.hexa.util.Utility;

public interface BlockSolution {
	
	HexaBlock getBlock();
	
	HexaGrid getGrid();
	
	int getRow();
	
	int getCol();
	
	public default String string() {
		return "row " + getRow() + "; " +
				" col " + getCol() + "; " +
				" block " + "\""+ Utility.printCells(getBlock().getCells()) + "\"; " +
				" grid " + "\""+ Utility.printCells(getGrid().getCells()) + "\"";
	}
}
