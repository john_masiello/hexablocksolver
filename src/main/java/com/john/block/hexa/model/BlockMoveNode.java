package com.john.block.hexa.model;

import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableInt;

import com.john.block.hexa.core.HexaBlock;
import com.john.block.hexa.util.SkipArrayIterator;

public class BlockMoveNode {
	
	private final ArrayList<BlockMove> aggMoves;
	private final MutableInt aggregateMoveIndex;
	private final MutableInt aggregateSortedIndex;
	private final MutableInt cellCost; 
	private SkipArrayIterator<HexaBlock> tieBreakIterator;
	private BlockMove blockMove;

	public BlockMoveNode() {
		aggMoves = new ArrayList<>();
		aggregateMoveIndex = new MutableInt(0);
		aggregateSortedIndex = new MutableInt(0);
		cellCost = new MutableInt(0);
	}

	public SkipArrayIterator<HexaBlock> getTieBreakIterator() {
		return tieBreakIterator;
	}

	public void setTieBreakIterator(SkipArrayIterator<HexaBlock> tieBreakIterator) {
		this.tieBreakIterator = tieBreakIterator;
	}

	public BlockMove getBlockMove() {
		return blockMove;
	}

	public void setBlockMove(BlockMove blockMove) {
		this.blockMove = blockMove;
	}

	public ArrayList<BlockMove> getAggMoves() {
		return aggMoves;
	}

	public MutableInt getAggregateMoveIndex() {
		return aggregateMoveIndex;
	}

	public MutableInt getAggregateSortedIndex() {
		return aggregateSortedIndex;
	}

	public MutableInt getCellCost() {
		return cellCost;
	}
}
