package com.john.block.hexa.model;

import com.john.block.hexa.core.HexaBlock;
import com.john.block.hexa.core.HexaGrid;

public class BlockMove implements BlockSolution {
	
	private HexaBlock block;
	private HexaGrid grid;
	private int blockIndex;
	private int row;
	private int col;
	private int shorelineImpact;
	private int islandImpact;
	
	public HexaBlock getBlock() {
		return block;
	}
	public BlockMove setBlock(HexaBlock block) {
		this.block = block;
		return this;
	}
	public HexaGrid getGrid() {
		return grid;
	}
	public BlockMove setGrid(HexaGrid grid) {
		this.grid = grid;
		return this;
	}
	public int getRow() {
		return row;
	}
	public BlockMove setRow(int row) {
		this.row = row;
		return this;
	}
	public int getCol() {
		return col;
	}
	public BlockMove setCol(int col) {
		this.col = col;
		return this;
	}
	public int getBlockIndex() {
		return blockIndex;
	}
	public BlockMove setBlockIndex(int blockIndex) {
		this.blockIndex = blockIndex;
		return this;
	}
	public int getShorelineImpact() {
		return shorelineImpact;
	}
	public BlockMove setShorelineImpact(int shorelineImpact) {
		this.shorelineImpact = shorelineImpact;
		return this;
	}
	public int getIslandImpact() {
		return islandImpact;
	}
	public BlockMove setIslandImpact(int islandImpact) {
		this.islandImpact = islandImpact;
		return this;
	}
}
