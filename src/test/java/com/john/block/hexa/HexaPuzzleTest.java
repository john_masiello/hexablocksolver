package com.john.block.hexa;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.john.block.hexa.core.HexaBlock;
import com.john.block.hexa.core.HexaGrid;
import com.john.block.hexa.core.HexaPuzzleGrid;
import com.john.block.hexa.core.HexaShapeOps;
import com.john.block.hexa.model.BlockMove;
import com.john.block.hexa.model.BlockSolution;
import com.john.block.hexa.util.SkipArray;
import com.john.block.hexa.util.Utility;

public class HexaPuzzleTest {
	private HexaPuzzle hexaPuzzle;
	
	@Before
	public void setUp() {
		final String[] strBlocks = new String[] {
				"01;10;01;10;01",
				"10;00;10;01",
				"01;10;01;10",
				"10;01;10",
				"10;01;10;01;10"
		};
		HexaBlock[] blocks = Arrays.stream(strBlocks)
			.map(Utility::cellsFromBinaryString)
			.map(HexaBlock::new)
			.collect(Collectors.toList()).toArray(new HexaBlock[strBlocks.length]);
		HexaGrid grid = new HexaGrid(Utility.cellsFromBinaryString(
				    "010000;"
				  + "101000;"
				  + "010000;"
				  + "101010;"
				  + "010101;"
				  + "101010;"
				  + "010101;"
				  + "101010;"
				  + "010000"));
		HexaPuzzleGrid puzzleGrid = new HexaPuzzleGrid(grid);
		hexaPuzzle = new HexaPuzzle(puzzleGrid, blocks);
		
	}
	
	@Test
	public void testConstructorGridInitializes() {
		List<HexaGrid> grids = hexaPuzzle.getPuzzleGrid().getHexaGrids();
		Assert.assertEquals(1, grids.size());
		Assert.assertEquals(20, grids.get(0).getNumberOfCells());
	}
	@Test
	public void testBlocksSortedByNumberOfCellsDesc() {
		SkipArray<HexaBlock> blocks = hexaPuzzle.getBlocks();
		Assert.assertEquals(5, blocks.size());
		
		for (int i = 1; i < blocks.size(); i++) {
			Assert.assertThat(blocks.get(i).getNumberOfCells(), 
					Matchers.lessThanOrEqualTo(blocks.get(i - 1).getNumberOfCells()));			
		}
	}
	@Test
	public void testFindBlockReturnsTrue() {
		HexaGrid grid = hexaPuzzle.getPuzzleGrid().getHexaGrids().get(0);
		HexaBlock block = hexaPuzzle.getBlocks().get(0);
		int rowIndex = 0;
		int colIndex = 0;
		Assert.assertTrue(hexaPuzzle.doesFit(grid, block, rowIndex, colIndex));
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testFindCorrectNumberOfWaysToPlaceBlockInGrid() {
		int blockIndex = 0;
		HexaBlock block = hexaPuzzle.getBlocks().get(blockIndex);
		System.out.println(Utility.printCells(block.getCells()));
		List<BlockMove> positions = hexaPuzzle.findAllPositions(blockIndex);
		Assert.assertEquals(6, positions.size());
		Assert.assertThat(positions, contains(
					both(hasProperty("row", equalTo(0))).and(hasProperty("col", equalTo(0))),
					both(hasProperty("row", equalTo(2))).and(hasProperty("col", equalTo(0))),
					both(hasProperty("row", equalTo(4))).and(hasProperty("col", equalTo(0))),
					both(hasProperty("row", equalTo(1))).and(hasProperty("col", equalTo(1))),
					both(hasProperty("row", equalTo(3))).and(hasProperty("col", equalTo(1))),
					both(hasProperty("row", equalTo(3))).and(hasProperty("col", equalTo(3)))
					));
	}
	
	@Test
	public void solveHexaPuzzle() {
		List<? extends BlockSolution> solutionSteps = hexaPuzzle.solve();
		Assert.assertNotNull(solutionSteps);
		Assert.assertThat(solutionSteps, not(Matchers.empty()));
		solutionSteps.forEach(sol -> System.out.println(sol.string()));
		solutionSteps.forEach(sol ->
				HexaShapeOps.xor(sol.getGrid(), sol.getBlock(), sol.getRow(), sol.getCol()));
		solutionSteps.forEach(sol -> {
			System.out.println("Emptied Grid: " + Utility.printCells(sol.getGrid().getCells()));
			Assert.assertEquals(true, HexaShapeOps.isEmpty(sol.getGrid()));
			});
	}
}
