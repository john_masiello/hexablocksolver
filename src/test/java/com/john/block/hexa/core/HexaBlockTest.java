package com.john.block.hexa.core;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.john.block.hexa.util.Utility;

public class HexaBlockTest {
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testBlock() {
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
				  "01;"
				+ "10;"
				+ "01;"
				+ "10;"
				+ "01"));
		Assert.assertEquals(5, block.getNumberOfCells());
		Assert.assertEquals(2, block.getWidth());
		Assert.assertEquals(6, block.getHeight());
	}
	@Test
	public void testBlock2() {
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
				  "10;"
				+ "00;"
				+ "10;"
				+ "01"));
		Assert.assertEquals(3, block.getNumberOfCells());
		Assert.assertEquals(2, block.getWidth());
		Assert.assertEquals(5, block.getHeight());
	}
	@Test
	public void testBlock3() {
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
				  "01;"
				+ "10;"
				+ "01;"
				+ "10"));
		Assert.assertEquals(4, block.getNumberOfCells());
		Assert.assertEquals(2, block.getWidth());
		Assert.assertEquals(5, block.getHeight());
	}
	@Test
	public void testBlock4() {
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
				  "10;"
				+ "01;"
				+ "10"));
		Assert.assertEquals(3, block.getNumberOfCells());
		Assert.assertEquals(2, block.getWidth());
		Assert.assertEquals(4, block.getHeight());
	}
	@Test
	public void testBlock5() {
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
				    "10;"
				  + "01;"
				  + "10;"
				  + "01;"
				  + "10"));
		Assert.assertEquals(5, block.getNumberOfCells());
		Assert.assertEquals(2, block.getWidth());
		Assert.assertEquals(6, block.getHeight());
	}
	@Test
	public void testGrid() {
		// Novice, Lvl 75
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
				    "010000;"
				  + "101000;"
				  + "010000;"
				  + "101010;"
				  + "010101;"
				  + "101010;"
				  + "010101;"
				  + "101010;"
				  + "010000"));
		Assert.assertEquals(20, block.getNumberOfCells());
		Assert.assertEquals(6, block.getWidth());
		Assert.assertEquals(10, block.getHeight());
		Assert.assertEquals(36, block.getShoreLineCount());
	}
	@Test
	public void testGrid2() {
		// Novice, Lvl 65
		HexaBlock block = new HexaBlock(Utility.cellsFromBinaryString(
			      "0001000;"
			    + "0010100;"
			    + "0101010;"
			    + "1010101;"
			    + "0101010;"
			    + "0010100;"
			    + "0101010"));
		Assert.assertEquals(18, block.getNumberOfCells());
		Assert.assertEquals(7, block.getWidth());
		Assert.assertEquals(8, block.getHeight());
		Assert.assertEquals(34, block.getShoreLineCount());
	}
	@Test
	public void testEmptyCellsFailsWithException() {
		exception.expect(IllegalStateException.class);
		exception.expectMessage("not tightly bound");
		new HexaBlock(Utility.cellsFromBinaryString(
				  ""));
	}
	@Test
	public void testEmptyCellsOnLeftFailsWithException() {
		exception.expect(IllegalStateException.class);
		exception.expectMessage("not tightly bound");
		new HexaBlock(Utility.cellsFromBinaryString(
				  "01;01;01"));
	}
	@Test
	public void testEmptyCellsOnRightFailsWithException() {
		exception.expect(IllegalStateException.class);
		exception.expectMessage("not tightly bound");
		new HexaBlock(Utility.cellsFromBinaryString(
				  "10;10;10"));
	}
	@Test
	public void testEmptyCellsOnTopFailsWithException() {
		exception.expect(IllegalStateException.class);
		exception.expectMessage("not tightly bound");
		new HexaBlock(Utility.cellsFromBinaryString(
				  "00;10;01"));
	}
	@Test
	public void testEmptyCellsOnBottomFailsWithException() {
		exception.expect(IllegalStateException.class);
		exception.expectMessage("not tightly bound");
		new HexaBlock(Utility.cellsFromBinaryString(
				  "10;01;00"));
	}
	@Test
	public void testNoCheckerboardFailsWithException() {
		exception.expect(IllegalStateException.class);
		exception.expectMessage("checker board");
		new HexaBlock(Utility.cellsFromBinaryString(
				  "10;11;10"));
	}
}
