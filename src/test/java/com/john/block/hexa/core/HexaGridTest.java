package com.john.block.hexa.core;

import org.junit.Assert;
import org.junit.Test;

import com.john.block.hexa.util.Utility;

public class HexaGridTest {
	@Test
	public void testIslandJustOne() {
		HexaGrid grid = new HexaGrid(Utility.cellsFromBinaryString(
				  "01010;"
				+ "10101;"
				+ "01010"));
		Assert.assertEquals(1, grid.getIslandCount());
	}
	@Test
	public void testIslandJustTwo() {
		HexaGrid grid = new HexaGrid(Utility.cellsFromBinaryString(
				  "01010;"
				+ "10001;"
				+ "01010"));
		Assert.assertEquals(2, grid.getIslandCount());
	}
}
