package com.john.block.hexa.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.john.block.hexa.util.Utility;

public class HexaShapeOpsTest {
	private HexaGrid grid;
	
	@Before
	public void setUp() {
		grid = new HexaGrid(Utility.cellsFromBinaryString("01;10"));
	}
	@Test
	public void testXorWholeGrid() {
		HexaShape block = new HexaBlock(grid.getCells());
		HexaShapeOps.xor(grid, block, 0, 0);
		Assert.assertEquals(0, grid.getNumberOfCells());
	}
	@Test
	public void testXorTwiceRestoresGrid() {
		HexaShape block = new HexaBlock(grid.getCells());
		HexaShapeOps.xor(grid, block, 0, 0);
		Assert.assertEquals(0, grid.getNumberOfCells());
		HexaShapeOps.xor(grid, block, 0, 0);
		Assert.assertEquals(2, grid.getNumberOfCells());
	}
	@Test
	public void testXorBlock() {
		HexaShape block = new HexaBlock(Utility.cellsFromBinaryString("1"));
		HexaShapeOps.xor(grid, block, 1, 0);
		Assert.assertEquals(1, grid.getNumberOfCells());
	}
	@Test
	public void testShoreLine() {
		Assert.assertEquals(10, grid.getShoreLineCount());
	}
	@Test
	public void testXorAfterCellsChanged() {
		HexaShape block = new HexaBlock(Utility.cellsFromBinaryString("1"));
		HexaShapeOps.xor(grid, block, 1, 0);
		Assert.assertEquals(6, grid.getShoreLineCount());
	}
}
